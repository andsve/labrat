_print = print
function print( ... )
	_print("[lua]", "m", ...)
end

print("Hello World")

--[[
f,e = physfs.open("test/boot.lua", "r")
if (not f) then
	print("could not open file")
	print( f, e )
else
	--print( f:read('*a') )
	--print( f:write("2"))
	local i = 1
	for line in f:lines() do
		print(i, line)
		i = i + 1
	end
	f:close()
end
]]

channels.on("coolevent", function ( ... )
	print("coolevent:", ... )
	channels.send("workerevent", "nope")
	worker.close()
end)

--channels.send("coolevent", "tello")


local new_thread = worker.spawn{ path = "test/testworker.lua" }
print("worker:", new_thread)
--new_thread:send("test")

--[[local coolevent = channels.get("coolevent")
coolevent:on(function ( ... )
	
end)]]


--[[
new_thread:on("message", function( data )
		print( data )
	end)

new_thread:post_message({aoeaoe})

]]