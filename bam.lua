-- build settings
CheckVersion("0.4")

local labrat = NewSettings()

labrat.cc.defines:Add("_DEBUG")

SetDriversClang( labrat )

-- adds libs and frameworkes to both static and shared linking tables
function add_libs( settings, lib )
	settings.link.libs:Add( lib )
	settings.dll.libs:Add( lib )
end

function add_frameworks( settings, lib )
	settings.link.frameworks:Add( lib )
	settings.dll.frameworks:Add( lib )
end

-- setup default libs and specific for each platform
if family == "unix" then
	if platform == "macosx" then
		add_frameworks( labrat, "OpenGL" )
		add_frameworks( labrat, "Cocoa" )
		add_frameworks( labrat, "IOKit" )
	end
elseif family == "windows" then
	add_libs( labrat, "opengl32" )
	add_libs( labrat, "glu32" )
	add_libs( labrat, "user32" )
	add_libs( labrat, "ADVAPI32" )
end

---------------------------------------------------------------------
-- labrat base
---------------------------------------------------------------------
labrat.cc.defines:Add("LABRAT_BUILD=1")

add_libs( labrat, "glfw3" )
add_libs( labrat, "physfs" )

-- im sorry ms jackson OOH
if family == "windows" then
	add_libs( labrat, "luajit_cool" )
else
	add_libs( labrat, "luajit-5.1.2.0.1" )
end

labrat.cc.includes:Add("include")


---------------------------------------------------------------------
-- binary output
---------------------------------------------------------------------
local labrat_bin = labrat:Copy()
labrat_bin.cc.Output = function(settings, filename) return PathJoin("build_bin", PathBase(string.gsub(filename, "%.%./", ""))) end

labrat_bin_src = { CollectRecursive("src/*.cpp"), CollectRecursive("src/*.c") }
labrat_bin_objs = Compile( labrat_bin, labrat_bin_src )


---------------------------------------------------------------------
-- library, static and shared, outputs
---------------------------------------------------------------------
local labrat_lib = labrat:Copy()
labrat_lib.cc.Output = function(settings, filename) return PathJoin("build_lib", PathBase(string.gsub(filename, "%.%./", ""))) end
labrat_lib.cc.defines:Add("LABRAT_LIBRARY=1")

labrat_lib_src = { CollectRecursive("src/*.cpp"), CollectRecursive("src/*.c") }
labrat_lib_objs = Compile( labrat_lib, labrat_lib_src )


---------------------------------------------------------------------
-- labrat targets
---------------------------------------------------------------------
PseudoTarget("lib", StaticLibrary(labrat_lib, "labrat", labrat_lib_objs) )
PseudoTarget("lib_shared", SharedLibrary(labrat_lib, "labrat", labrat_lib_objs) )
PseudoTarget("bin", Link(labrat_bin, "labrat", labrat_bin_objs) )
DefaultTarget("bin")

