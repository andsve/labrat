#ifndef _LABRAT_KEYREG_H_
#define _LABRAT_KEYREG_H_


extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

namespace labrat
{
	
	bool _labrat_keyreg_is_valid();
	int _labrat_keyreg_validate(lua_State* L);

}

#endif
