#ifndef _LABRAT_UI_CORE_
#define _LABRAT_UI_CORE_

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

namespace labrat
{
	namespace ui
	{
		
		int init( lua_State *L );

	}
}

#endif