#ifndef _LABRAT_LOG_H_
#define _LABRAT_LOG_H_

#define LOG(_level, ...) log(_level, __FILE__, __LINE__, __VA_ARGS__)
#if defined(WIN32) || defined(_WIN32) || defined(WIN64) || defined(CONF_FAMILY_WINDOWS)
	#define LOG_INFO(...) log(LOG_INFO, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
	#ifdef _DEBUG
		#define LOG_DEBUG(...) log(LOG_DEBUG, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
	#else
		#define LOG_DEBUG(...)
	#endif
	#define LOG_ERROR(...) log(LOG_ERROR, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
	#define LOG_WARNING(...) log(LOG_WARNING, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#else
	#define LOG_INFO(...) log(LOG_INFO, __FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)
	#ifdef _DEBUG
		#define LOG_DEBUG(...) log(LOG_DEBUG, __FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)
	#else
		#define LOG_DEBUG(...)
	#endif
	#define LOG_ERROR(...) log(LOG_ERROR, __FILE__, __LINE__, __PRETTY_FUNCTION__, __VA_ARGS__)
	#define LOG_WARNING(...) log(LOG_WARNING, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)
#endif

namespace labrat
{
	enum LogLevel { LOG_INFO, LOG_DEBUG, LOG_WARNING, LOG_ERROR };
	void log(LogLevel _level, const char* _file, int _fileno, const char* _scope, const char* _message, ...);
	
} /* labrat */

#endif