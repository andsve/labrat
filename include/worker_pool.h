#ifndef _LABRAT_WORKER_POOL_H_
#define _LABRAT_WORKER_POOL_H_

#include "worker.h"
#include <tinythread.h>
#include <lua.h>
#include <lauxlib.h>

#define LABRAT_POOL_SIZE 32

namespace labrat
{
	class worker_pool
	{
	public:
		worker_pool();
		virtual ~worker_pool();
	
		void add(worker* _worker);
		void join_all();

	private:
		// todo use a better pool then a static array
		unsigned int current_pool_index;
		worker* pool[LABRAT_POOL_SIZE];
		tthread::mutex pool_mutex;
	};

	//int luaopen_workerpool(lua_State* L);
}

#endif