#ifndef _LABRAT_CHANNELS_H_
#define _LABRAT_CHANNELS_H_

#include "labrat.h"
#include "log.h"
#include "rataux.h"
#include "worker.h"
#include "tinythread.h"
#include <vector>

#ifdef _WIN32
	#include <unordered_map>
#else
	#include <tr1/unordered_map>
#endif

#include <string>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}


namespace labrat
{
	class channel;

	// we need a channel message struct so we only
	// have one instance for each message. we alse need
	// to count references in some way that does not block...
	class channel_message
	{
	public:
		channel_message( const char* _message, size_t _size )
		{
			// todo create message with new
			// copy string !
			message = new char[_size+1]; // +1 -> ensure string is terminated
			strncpy( (char*) message,_message,_size);
			message[_size] = '\0';

			ref = 0;
		};
		~channel_message()
		{
			m.lock();
			if (ref != 0)
			{
				LOG_ERROR( "Deleting a channel_message with ref != 0! (ref: %d).", ref );
			}
			m.unlock();
			delete message; 
		}

		void ref_add() { m.lock(); ref += 1; m.unlock(); };
		int ref_remove() { m.lock(); ref -= 1; m.unlock(); return ref; };

		char* message;
		tthread::mutex m;
		unsigned int ref;
	};

	class channel_connection
	{
	public:
		channel_connection(worker* _w, int _func_id, channel* _c) { w = _w; func_id = _func_id; c = _c; w->add_channel_connection(this); };

		void add_message( channel_message* _message )
		{
			m.lock();
			_message->ref_add();
			messages.push_back( _message );
			m.unlock();


			// signal on conditional
			this->w->signal();
		};

		void purge_messages()
		{
			m.lock();
			

			std::vector<channel_message*>::iterator it;
			int ref;

			//loop through messages, call lua func for each message.
			for (it = messages.begin(); it != messages.end(); ++it)
			{

				lua_rawgeti( w->L, LUA_REGISTRYINDEX, func_id );
				lua_pushstring( w->L, (*it)->message );
				int s = lua_pcall( w->L, 1, 0, 0 );
				if (s)
				{
					return;
				}

				// remove reference, if reference count reaches zero => delete message
				ref = (*it)->ref_remove();
				if (ref == 0)
				{
					delete (*it);
				}
			}

			messages.clear();
			
			m.unlock();
		};

		worker* w;
		int func_id;
		channel* c;

		// buffered messages on this channel connection
		std::vector<channel_message*> messages;
		tthread::mutex m;
	};

	channel* _labrat_get_channel(std::string _name);

	class channel
	{
	public:
		channel(const char* _name) { name = _name; };
		~channel() {};

		bool add(channel_connection* _connection);
		void push(const char* _msg, size_t _size);
		bool unregister(worker* _worker);
		
	private:
		std::string name;
		std::vector<channel_connection*> connections;
		tthread::mutex m;

	};

	int luaopen_labrat_channels(lua_State* L);
}

#endif