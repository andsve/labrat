#ifndef _LABRAT_WORKER_H_
#define _LABRAT_WORKER_H_


#include <tinythread.h>

#ifdef _WIN32
	#include <unordered_map>
#else
	#include <tr1/unordered_map>
#endif

#include <vector>
//using namespace tthread;

#include "labrat.h"
#include "rataux.h"
#include "physfs.h"

namespace labrat
{
	class worker;
	class channel;
	class channel_connection;

	struct worker_event_refs
	{
		int on_message;
		worker_event_refs() { on_message = LUA_NOREF; }
	};

	struct worker_message
	{
		worker* sender;
		const char* data;
		worker_message(worker* _sender, const char* _data) { sender = _sender; data = _data; };
	};

	class worker
	{
	public:
		worker( worker* _parent, char* _path = NULL, labrat_lua_cb _register_external_cb = NULL );
		virtual ~worker();
		void run_path(const char * _path);
		void run_raw(const char * _raw);
		void join() { if (!t) return; t->join(); };
		void add_channel_connection( channel_connection* _connection );
		//void send_message(const char* _channel_name, const char* _msg);
		//void bind_event( int _ref ); // todo send a event type here, now it defaults to 'message'

		// getters / setters
		//bool add_child(worker* _child);
		//bool remove_child(worker* _child);
		unsigned int get_depth() { return depth; }
		worker* get_parent() { return parent; }
		const char* get_path() { return path; }
		void register_extra();

		void signal();
		void stop() { running = false; };
		
		lua_State* L;
		labrat_lua_cb register_external_cb;
	
	private:
		static void _run(void *_this) { ((worker*)_this)->run(); };
		void run();

		bool init();
		void register_base();

		//void register_keyreg();

		void trigger_events();
		std::vector<channel_connection*> connections;
		tthread::mutex connection_mutex;
		tthread::mutex message_wait_mutex;

		//std::tr1::unordered_map<std::string, labrat::channel*> channels;
		//fifo_buffer<worker_message*> message_queue;
		//worker_event_refs event_refs;
		//tthread::mutex events_mutex;

		bool valid;
		bool running;
		worker* parent;
		char* path;
		PHYSFS_sint64 raw_data_length;
		char* raw_data;
		tthread::thread *t;
		unsigned int depth; // spawn depth

		unsigned int message_count;

		tthread::condition_variable message_cond;
	};

	int luaopen_workers(lua_State* L);

}

#endif