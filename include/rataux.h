#ifndef _LABRAT_RATAUX_H_
#define _LABRAT_RATAUX_H_

#include <tinythread.h>

namespace labrat
{
	template <typename T>
	class fifo_buffer
	{
	public:
		fifo_buffer()
		{
			tail = NULL;
			head = NULL;
			size = 0;
		};

		virtual ~fifo_buffer() {
			//delete [] buffer;
		};

		void push(T _new_value)
		{
			m.lock();
			_fifo_entry* e = new _fifo_entry( _new_value, tail, NULL );
			if (tail != NULL)
				tail->prev = e;
			tail = e;
			if (head == NULL)
				head = tail;
			size += 1;
			m.unlock();
		};

		T pop()
		{
			T r = NULL;
			m.lock();
			if (head != NULL)
			{			
				_fifo_entry* e = head;
				r = e->data;
				if (e->prev != NULL)
					e->prev->next = NULL;
				head = e->prev;
				delete e;
				size -= 1;
			}
			m.unlock();

			return r;
		};
	
	private:
		struct _fifo_entry
		{
			T data;
			_fifo_entry* next;
			_fifo_entry* prev;
			_fifo_entry(T _data, _fifo_entry* _next, _fifo_entry* _prev) { data = _data; next = _next; prev = _prev; };
		};
		size_t size;
		// push -> [ tail ... head ] -> pop
		_fifo_entry* tail;
		_fifo_entry* head;
		tthread::mutex m;
	};
}

#endif