#ifndef _LABRAT_LABRAT_H_
#define _LABRAT_LABRAT_H_

	#ifndef LABRAT_APP_DEFAULT_NAME
		#define LABRAT_APP_DEFAULT_NAME "test"
	#endif

	#ifndef LABRAT_APP_LUA_FIELD
		#define LABRAT_APP_LUA_FIELD LABRAT_APP_DEFAULT_NAME
	#endif

	#define LABRAT_MAX_PATH 1024
	#define LABRAT_MAX_CHILD_WORKERS 4
	#define LABRAT_MAX_WORKER_DEPTH 4
	#define LABRAT_VERSION "0.0.8"


extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

typedef int (*labrat_lua_cb)(lua_State*);

namespace labrat
{
	class worker_pool;
	extern worker_pool* wp;


	void run( const char* _bin_name, const char* _app_directory_name, labrat_lua_cb _lua_extras = NULL);
	void join_workers();
}

#endif
