# Labrat
I'm the labrat.

This is a separate take on Fabric, a project that could be considered 'What Fabric 3 should have been'.
The aim is to combine the successful parts of 'Toolbox' and 'Fabric' into a pure gold project.

## Planned Features
* Low level
	* Focus on tools and graphical applications
	* Event driven application flow
	* Concurrency support via multiple LuaJIT VMs, called Worker Threads
	* Message passing as communication between Worker threads
	* Optional: Dedicated rendering thread using 'packet based rendering'
* High level
	* OpenGL powered GUI system based on ideas from Blender
	* Optional: Native GUI support via Qt

## Dependencies
* [GLFW3](https://github.com/elmindreda/glfw)
* [LuaJIT](http://luajit.org/)
* [PhysFS 2.0.3](http://hg.icculus.org/icculus/physfs/)

Ideally Labrat should be linked statically with GLFW, but dynamically with LuaJIT, due to LuaJIT-FFI not working if built statically on Windows.

## Building
Make sure you have the latest dependencies, then build using [bam](http://matricks.github.io/bam/) like this: `bam -r bcs`