#include "labrat.h"
#include <stdio.h>
#include <cstdlib>
#include <GLFW/glfw3.h>
#include <physfs.h>

#include "worker_pool.h"
#include "worker.h"
#include "log.h"

// microsoft huh..
#ifdef _WIN32
	#define snprintf _snprintf
#endif

using namespace labrat;

worker_pool* labrat::wp;

void static _glfw_error_cb(int error, const char* description)
{
    LOG_ERROR("GLFW Error: %s", description);
}

void static _physfs_setup(char const *_bin_name)
{
	// setup physfs
	if (PHYSFS_init(_bin_name) == 0)
	{
		LOG_ERROR("PhysFS Init Error: %s", PHYSFS_getLastError());
	}

	if (PHYSFS_setSaneConfig("PixelFolders", "labrat", "patch", 0, 1) == 0)
	{
		LOG_ERROR("PhysFS Config Error: %s", PHYSFS_getLastError());
	}	
	
	if (PHYSFS_mount(_bin_name, NULL, 1) == 0)
	{
		LOG_ERROR("PhysFS Mount Error for '%s': %s", _bin_name, PHYSFS_getLastError());
	}

	if (PHYSFS_mount("./", NULL, 1) == 0)
	{
		LOG_ERROR("PhysFS Mount Error for './': %s", PHYSFS_getLastError());
	}

	if (PHYSFS_setWriteDir("./") == 0)
	{
		LOG_ERROR("PhysFS Config Error: %s", PHYSFS_getLastError());
	}
}

void labrat::run( const char* _bin_name, const char* _app_directory_name, labrat_lua_cb _lua_extras )
{
	_physfs_setup( _bin_name );

	// get the applications boot path
	char t_application_boot_path[LABRAT_MAX_PATH];

	// append boot.lua to boot path
	snprintf(t_application_boot_path, LABRAT_MAX_PATH, "%s/%s",
		_app_directory_name, "boot.lua");

	// setup worker pool
	wp = new worker_pool();
	wp->add(new worker( NULL, t_application_boot_path, _lua_extras ));
}

void labrat::join_workers()
{
	wp->join_all();
}

#ifdef LABRAT_LIBRARY
LOL
#else

int main(int argc, char const *argv[])
{
	LOG_INFO("Labrat v%sb%u", LABRAT_VERSION, LABRAT_BUILD);

	const char* t_application_directory_name = LABRAT_APP_DEFAULT_NAME;
	if (argc > 1)
	{
		t_application_directory_name = argv[1];
	}
	labrat::run( argv[0], t_application_directory_name );

	// hack to run binary without application bundle
/*#if PLATFORM_TOOLBOX == PLATFORM_MAC
    ProcessSerialNumber PSN;
    GetCurrentProcess(&PSN);
    TransformProcessType(&PSN,kProcessTransformToForegroundApplication);
#endif*/

    glfwSetErrorCallback( _glfw_error_cb );
	
	if (!glfwInit())
		exit(EXIT_FAILURE);

	GLFWwindow* window = glfwCreateWindow(640, 480, "Labrat Test", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	while (!glfwWindowShouldClose(window))
	{


		// Keep running
		glfwPollEvents();

		glClearColor(1,0,0,1);
		glClear( GL_COLOR_BUFFER_BIT );
		glfwSwapBuffers(window);
	}

	glfwDestroyWindow(window);

	LOG_DEBUG("Window closed, will wait for workers now.");

	/*worker w2(&w, "aoe");
	worker w3(&w2, "iueieu");
	w.join();
	w2.join();
	w3.join();*/

	//printf("%s\n", t_application_boot_path);

	
	/*

	GLFWwindow* window = glfwCreateWindow(640, 480, "Labrat 0.1.1", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);

	while (!glfwWindowShouldClose(window))
	{
		// Keep running
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	*/
    glfwTerminate();

    labrat::join_workers();

    if (PHYSFS_deinit() == 0)
    {
    	LOG_ERROR("PhysFS Error: %s",  PHYSFS_getLastError());
    }

	return 0;
}

#endif
