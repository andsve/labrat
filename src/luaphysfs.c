#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "luaphysfs.h"

#include <physfs.h>

#define phys_INPUT        1
#define phys_OUTPUT       2
#define LUA_PHYSFSFHANDLE "PHYSFS_File*"


static const char *const fnames[] = {"input", "output"};


static int pushresult (lua_State *L, int i, const char *filename) {
  int en = errno;  /* calls to Lua API may change this value */
  if (i) {
    lua_pushboolean(L, 1);
    return 1;
  }
  else {
    lua_pushnil(L);
    if (filename)
    {
      lua_pushfstring(L, "%s: %s", filename, PHYSFS_getLastError());
    } else {
      lua_pushfstring(L, "%s", PHYSFS_getLastError());
    }
    return 2;
  }
}


static void fileerror (lua_State *L, int arg, const char *filename) {
  lua_pushfstring(L, "%s: %s", filename, PHYSFS_getLastError());
  luaL_argerror(L, arg, lua_tostring(L, -1));
}


#define topfile(L)      ((PHYSFS_File **)luaL_checkudata(L, 1, LUA_PHYSFSFHANDLE))


static int phys_type (lua_State *L) {
  void *ud;
  luaL_checkany(L, 1);
  ud = lua_touserdata(L, 1);
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_PHYSFSFHANDLE);
  if (ud == NULL || !lua_getmetatable(L, 1) || !lua_rawequal(L, -2, -1))
    lua_pushnil(L);  /* not a file */
  else if (*((PHYSFS_File **)ud) == NULL)
    lua_pushliteral(L, "closed physfs file");
  else
    lua_pushliteral(L, "physfs file");
  return 1;
}


static PHYSFS_File *tofile (lua_State *L) {
  PHYSFS_File **f = topfile(L);
  if (*f == NULL)
    luaL_error(L, "attempt to use a closed file");
  return *f;
}



/*
** When creating file handles, always creates a `closed' file handle
** before opening the actual file; so, if there is a memory error, the
** file is not left opened.
*/
static PHYSFS_File **newfile (lua_State *L) {
  PHYSFS_File **pf = (PHYSFS_File **)lua_newuserdata(L, sizeof(PHYSFS_File *));
  *pf = NULL;  /* file handle is currently `closed' */
  luaL_getmetatable(L, LUA_PHYSFSFHANDLE);
  lua_setmetatable(L, -2);
  return pf;
}


/*
** this function has a separated environment, which defines the
** correct __close for 'popen' files
*/
//static int phys_pclose (lua_State *L) {
//  PHYSFS_File **p = topfile(L);
//  int ok = lua_pclose(L, *p);
//  if (ok) *p = NULL;
//  return pushresult(L, ok, NULL);
//}
//
//
//static int phys_fclose (lua_State *L) {
//  PHYSFS_File **p = topfile(L);
//  int ok = (fclose(*p) == 0);
//  if (ok) *p = NULL;
//  return pushresult(L, ok, NULL);
//}
//

static int aux_close (lua_State *L) {
  lua_getfenv(L, 1);
  lua_getfield(L, -1, "__close");
  return (lua_tocfunction(L, -1))(L);
}


static int phys_close (lua_State *L) {
  PHYSFS_File **p = topfile(L);
  int ok = (PHYSFS_close(*p) != 0);
  if (ok) *p = NULL;
  return pushresult(L, ok, NULL);
}


static int phys_gc (lua_State *L) {
  PHYSFS_File *f = *topfile(L);
  PHYSFS_close(f);
  //phys_close(L);
  return 0;
}


static int phys_tostring (lua_State *L) {
  PHYSFS_File *f = *topfile(L);
  if (f == NULL)
    lua_pushstring(L, "physfs file (closed)");
  else
    lua_pushfstring(L, "physfs file (%p)", f);
  return 1;
}


static int phys_open (lua_State *L) {
  const char *filename = luaL_checkstring(L, 1);
  const char *mode = luaL_optstring(L, 2, "r");
  PHYSFS_File **pf = NULL;

  // TODO handle "b" option
  if (mode[0] == 'r')
  {
    if (mode[1] == '+')
    {
      if (PHYSFS_exists(filename))
      {
        pf = newfile(L);
        *pf = PHYSFS_openAppend(filename);
        if (*pf != NULL)
        {
          PHYSFS_seek(*pf, 0);
        }
      }

    } else {
      pf = newfile(L);
      *pf = PHYSFS_openRead(filename);
    }

  } else if (mode[0] == 'w') {
    // TODO handle "+" option in some way
    pf = newfile(L);
    *pf = PHYSFS_openWrite(filename);

  } else if (mode[0] == 'a') {
    // TODO handle "+" option in some way
    pf = newfile(L);
    *pf = PHYSFS_openAppend(filename);
  }

  //PHYSFS_ErrorCode errc = PHYSFS_getLastErrorCode();
  //if (errc != PHYSFS_ERR_OK)
  if (*pf == NULL)
  {
    //PHYSFS_setErrorCode(errc);
    return pushresult(L, 0, filename);
  } else {
    return 1;
  }
  //return (*pf == NULL) ? pushresult(L, 0, filename) : 1;
}


//static int phys_popen (lua_State *L) {
//  const char *filename = luaL_checkstring(L, 1);
//  const char *mode = luaL_optstring(L, 2, "r");
//  PHYSFS_File **pf = newfile(L);
//  *pf = lua_popen(L, filename, mode);
//  return (*pf == NULL) ? pushresult(L, 0, filename) : 1;
//}
//
//
//static int phys_tmpfile (lua_State *L) {
//  PHYSFS_File **pf = newfile(L);
//  *pf = tmpfile();
//  return (*pf == NULL) ? pushresult(L, 0, NULL) : 1;
//}
//
//
//static PHYSFS_File *getiofile (lua_State *L, int findex) {
//  PHYSFS_File *f;
//  lua_rawgeti(L, LUA_ENVIRONINDEX, findex);
//  f = *(PHYSFS_File **)lua_touserdata(L, -1);
//  if (f == NULL)
//    luaL_error(L, "standard %s file is closed", fnames[findex - 1]);
//  return f;
//}
//
//
//static int g_iofile (lua_State *L, int f, const char *mode) {
//  if (!lua_isnoneornil(L, 1)) {
//    const char *filename = lua_tostring(L, 1);
//    if (filename) {
//      PHYSFS_File **pf = newfile(L);
//      *pf = fopen(filename, mode);
//      if (*pf == NULL)
//        fileerror(L, 1, filename);
//    }
//    else {
//      tofile(L);  /* check that it's a valid file handle */
//      lua_pushvalue(L, 1);
//    }
//    lua_rawseti(L, LUA_ENVIRONINDEX, f);
//  }
//  /* return current value */
//  lua_rawgeti(L, LUA_ENVIRONINDEX, f);
//  return 1;
//}
//
//
//static int phys_input (lua_State *L) {
//  return g_iofile(L, phys_INPUT, "r");
//}
//
//
//static int phys_output (lua_State *L) {
//  return g_iofile(L, phys_OUTPUT, "w");
//}


static int phys_readline (lua_State *L);


static void aux_lines (lua_State *L, int idx, int toclose) {
  lua_pushvalue(L, idx);
  lua_pushboolean(L, toclose);  /* close/not close file when finished */
  lua_pushcclosure(L, phys_readline, 2);
}


static int phys_lines (lua_State *L) {
  tofile(L);  /* check that it's a valid file handle */
  aux_lines(L, 1, 0);
  return 1;
}


//static int phys_lines (lua_State *L) {
//  if (lua_isnoneornil(L, 1)) {  /* no arguments? */
//    /* will iterate over default input */
//    lua_rawgeti(L, LUA_ENVIRONINDEX, phys_INPUT);
//    return phys_lines(L);
//  }
//  else {
//    const char *filename = luaL_checkstring(L, 1);
//    PHYSFS_File **pf = newfile(L);
//    *pf = fopen(filename, "r");
//    if (*pf == NULL)
//      fileerror(L, 1, filename);
//    aux_lines(L, lua_gettop(L), 1);
//    return 1;
//  }
//}


/*
** {======================================================
** READ
** =======================================================
*/

//static int read_number (lua_State *L, PHYSFS_File *f) {
//  lua_Number d;
//  if (fscanf(f, LUA_NUMBER_SCAN, &d) == 1) {
//    lua_pushnumber(L, d);
//    return 1;
//  }
//  else return 0;  /* read fails */
//}


static int test_eof (lua_State *L, PHYSFS_File *f) {
  int eof = PHYSFS_eof(f);
  lua_pushlstring(L, NULL, 0);
  return eof;
  /*int c = getc(f);
  ungetc(c, f);
  lua_pushlstring(L, NULL, 0);
  return (c != EOF);*/
}


static int read_line (lua_State *L, PHYSFS_File *f) {
  luaL_Buffer b;
  luaL_buffinit(L, &b);
  for (;;) {
    size_t l;
    char *p;

    if (PHYSFS_eof(f)) {
      luaL_pushresult(&b);  // close buffer
      return (lua_strlen(L, -1) > 0);  // check whether read something
    }
    p = luaL_prepbuffer(&b);
    PHYSFS_read( f, p, 1, 1);
    l = strlen(p);
    if (p[0] != '\n')
      luaL_addsize(&b, 1);
    else {
      //luaL_addsize(&b, l - 1);  // do not include `eol'
      luaL_pushresult(&b);  // close buffer
      return 1;  // read at least an `eol'
    }

  }
}


static int read_chars (lua_State *L, PHYSFS_File *f, size_t n) {
  size_t rlen;  /* how much to read */
  size_t nr;  /* number of chars actually read */
  luaL_Buffer b;
  luaL_buffinit(L, &b);
  rlen = LUAL_BUFFERSIZE;  /* try to read that much each time */
  do {
    char *p = luaL_prepbuffer(&b);
    if (rlen > n) rlen = n;  /* cannot read more than asked */
    //nr = fread(p, sizeof(char), rlen, f);
    nr = PHYSFS_read ( f, p, 1, rlen );
    luaL_addsize(&b, nr);
    n -= nr;  /* still have to read `n' chars */
  } while (n > 0 && nr == rlen);  /* until end of count or eof */
  luaL_pushresult(&b);  /* close buffer */
  return (n == 0 || lua_strlen(L, -1) > 0);
}


static int g_read (lua_State *L, PHYSFS_File *f, int first) {
  int nargs = lua_gettop(L) - 1;
  int success;
  int n;
  //clearerr(f);
  if (nargs == 0) {  /* no arguments? */
    success = read_line(L, f);
    n = first+1;  /* to return 1 result */
  }
  else {  /* ensure stack space for all results and for auxlib's buffer */
    luaL_checkstack(L, nargs+LUA_MINSTACK, "too many arguments");
    success = 1;
    for (n = first; nargs-- && success; n++) {
      if (lua_type(L, n) == LUA_TNUMBER) {
        size_t l = (size_t)lua_tointeger(L, n);
        success = (l == 0) ? test_eof(L, f) : read_chars(L, f, l);
      }
      else {
        const char *p = lua_tostring(L, n);
        luaL_argcheck(L, p && p[0] == '*', n, "invalid option");
        switch (p[1]) {
          case 'n':  /* number */
            //success = read_number(L, f);
            //break;
            return luaL_argerror(L, n, "number read not supported");
          case 'l':  /* line */
            success = read_line(L, f);
            break;
          case 'a':  /* file */
            read_chars(L, f, ~((size_t)0));  /* read MAX_SIZE_T chars */
            success = 1; /* always success */
            break;
          default:
            return luaL_argerror(L, n, "invalid format");
        }
      }
    }
  }
  //PHYSFS_ErrorCode errc = PHYSFS_getLastErrorCode();
  //if (errc != PHYSFS_ERR_OK) {
  /*if ()
    //ferror(f))
    //PHYSFS_setErrorCode(errc);
    return pushresult(L, 0, NULL);
  }*/
  if (!success) {
    lua_pop(L, 1);  /* remove last result */
    lua_pushnil(L);  /* push nil instead */
  }
  return n - first;
}

//
//static int phys_read (lua_State *L) {
//  return g_read(L, getiofile(L, phys_INPUT), 1);
//}
//

static int phys_read (lua_State *L) {
  return g_read(L, tofile(L), 2);
}


static int phys_readline (lua_State *L) {
  PHYSFS_File *f = *(PHYSFS_File **)lua_touserdata(L, lua_upvalueindex(1));
  int sucess;
  if (f == NULL)  /* file is already closed? */
    luaL_error(L, "file is already closed");
  sucess = read_line(L, f);
  //if (ferror(f))
  //  return luaL_error(L, "%s", strerror(errno));
  if (sucess) return 1;
  else {  /* EOF */
    if (lua_toboolean(L, lua_upvalueindex(2))) {  /* generator created file? */
      lua_settop(L, 0);
      lua_pushvalue(L, lua_upvalueindex(1));
      aux_close(L);  /* close it */
    }
    return 0;
  }
}

///* }====================================================== */
//

static int g_write (lua_State *L, PHYSFS_File *f, int arg) {
  int nargs = lua_gettop(L) - 1;
  int status = 1;
  for (; nargs--; arg++) {
    if (lua_type(L, arg) == LUA_TNUMBER) {
      /* optimization: could be done exactly as for strings */
      /*status = status &&
          fprintf(f, LUA_NUMBER_FMT, lua_tonumber(L, arg)) > 0;
          */
      size_t l;
      char s[LUAL_BUFFERSIZE];
      sprintf(s, LUA_NUMBER_FMT, lua_tonumber(L, arg));
      l = strlen(s);
      status = status && (PHYSFS_write( f, s, 1, l) == l);
    }
    else {
      size_t l;
      const char *s = luaL_checklstring(L, arg, &l);
      status = status && (PHYSFS_write( f, s, 1, l) == l);
    }
  }
  return pushresult(L, status, NULL);
}


//static int phys_write (lua_State *L) {
//  return g_write(L, getiofile(L, phys_OUTPUT), 1);
//}


static int phys_write (lua_State *L) {
  return g_write(L, tofile(L), 2);
}


static int phys_seek (lua_State *L) {
  static const int mode[] = {SEEK_SET, SEEK_CUR, SEEK_END};
  static const char *const modenames[] = {"set", "cur", "end", NULL};
  PHYSFS_File *f = tofile(L);
  int op = luaL_checkoption(L, 2, "cur", modenames);

  long offset = luaL_optlong(L, 3, 0);
  if (op == SEEK_SET)
    op = PHYSFS_seek(f, offset);
  else if (op == SEEK_CUR)
    op = PHYSFS_seek(f, PHYSFS_tell(f) + offset);
  else
    op = PHYSFS_seek(f, PHYSFS_fileLength(f) + offset);
  //op = fseek(f, offset, mode[op]);
  if (!op)
    return pushresult(L, 0, NULL);  /* error */
  else {
    lua_pushinteger(L, PHYSFS_tell(f));
    return 1;
  }
}

//
//static int phys_setvbuf (lua_State *L) {
//  static const int mode[] = {_IONBF, _IOFBF, _IOLBF};
//  static const char *const modenames[] = {"no", "full", "line", NULL};
//  PHYSFS_File *f = tofile(L);
//  int op = luaL_checkoption(L, 2, NULL, modenames);
//  lua_Integer sz = luaL_optinteger(L, 3, LUAL_BUFFERSIZE);
//  int res = setvbuf(f, NULL, mode[op], sz);
//  return pushresult(L, res == 0, NULL);
//}
//
//
//
//static int phys_flush (lua_State *L) {
//  return pushresult(L, fflush(getiofile(L, phys_OUTPUT)) == 0, NULL);
//}
//
//
static int phys_flush (lua_State *L) {
  return pushresult(L, PHYSFS_flush(tofile(L)), NULL);
}


static const luaL_Reg iolib[] = {
  //{"close", phys_close},
  //{"flush", phys_flush},
  //{"input", phys_input},
  //{"lines", phys_lines},
  {"open", phys_open},
  //{"output", phys_output},
  //{"popen", phys_popen},
  //{"read", phys_read},
  //{"tmpfile", phys_tmpfile},
  {"type", phys_type},
  //{"write", phys_write},
  {NULL, NULL}
};


static const luaL_Reg flib[] = {
  {"close", phys_close},
  {"flush", phys_flush},
  {"lines", phys_lines},
  {"read", phys_read},
  {"seek", phys_seek},
  //{"setvbuf", phys_setvbuf},
  {"write", phys_write},
  {"__gc", phys_gc},
  {"__tostring", phys_tostring},
  {NULL, NULL}
};


static void createmeta (lua_State *L) {
  luaL_newmetatable(L, LUA_PHYSFSFHANDLE);  /* create metatable for file handles */
  lua_pushvalue(L, -1);  /* push metatable */
  lua_setfield(L, -2, "__index");  /* metatable.__index = metatable */
  luaL_register(L, NULL, flib);  /* file methods */
}


/*static void createstdfile (lua_State *L, PHYSFS_File *f, int k, const char *fname) {
  *newfile(L) = f;
  if (k > 0) {
    lua_pushvalue(L, -1);
    lua_rawseti(L, LUA_ENVIRONINDEX, k);
  }
  lua_setfield(L, -2, fname);
}*/


int luaopen_physfs (lua_State *L) {
  createmeta(L);
  /* create (private) environment (with fields phys_INPUT, phys_OUTPUT, __close)
  lua_createtable(L, 2, 1);
  lua_replace(L, LUA_ENVIRONINDEX);*/
  /* open library */
  luaL_register(L, "physfs", iolib);
  /* create (and set) default files
  createstdfile(L, stdin, phys_INPUT, "stdin");
  createstdfile(L, stdout, phys_OUTPUT, "stdout");
  createstdfile(L, stderr, 0, "stderr"); */
  /* create environment for 'popen' 
  lua_getfield(L, -1, "popen");
  lua_createtable(L, 0, 1);
  lua_pushcfunction(L, phys_pclose);
  lua_setfield(L, -2, "__close");
  lua_setfenv(L, -2);
  lua_pop(L, 1);   pop 'popen' */
  /* set default close function 
  lua_pushcfunction(L, phys_fclose);
  lua_setfield(L, LUA_ENVIRONINDEX, "__close");*/
  return 1;
}
