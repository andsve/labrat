#include "worker.h"
#include "channels.h"
#include "keyreg.h"
#include "log.h"
#include "worker_pool.h"

extern "C" {
	#include "luaphysfs.h"
}

#include <physfs.h>

using namespace labrat;
using namespace tthread;

#define LUA_WORKERHANDLE "worker*"
#define toworker(L)      ((worker **)luaL_checkudata(L, 1, LUA_WORKERHANDLE))

worker::worker( worker* _parent, char* _path, labrat_lua_cb _register_external_cb )
	: path(_path)
	, register_external_cb(_register_external_cb)
	, parent(_parent)
	, valid(false)
	, running(true)
{
	if (!_parent)
	{
		depth = 0;
	} else {
		depth = _parent->get_depth() + 1;
	}
	//LOG_DEBUG("Creating worker with depth: %u", depth);

	// clear children array
	/*for (int i = 0; i < LABRAT_MAX_CHILD_WORKERS; ++i)
	{
		children[i] = NULL;
	}*/

	// add self to parent
	/*if (parent)
	{
		parent->add_child(this);
	}*/

	// todo: check if depth is more than max worker depth
	// if (depth >= LABRAT_MAX_WORKER_DEPTH) { ... }	

	// run directly if a path was supplied
	if (_path != NULL)
		run_path(_path);
}

worker::~worker()
{
	this->join();
	delete this->t;

	if (raw_data)
		delete [] raw_data;

	if (path)
		delete [] path;
}

void worker::run_path(const char* _path)
{
	// store path
	size_t l = strlen(_path);
	path = new char[l+1]();
	strncpy( path, _path, l );
	path[l] = '\0';

	// load script via physfs
	PHYSFS_File *f = PHYSFS_openRead( path );
	if (f == NULL)
	{
		LOG_ERROR( "Could not open %s as worker.", path );
		return;
	}

	raw_data_length = PHYSFS_fileLength( f );
	raw_data = new char[raw_data_length]();

	PHYSFS_sint64 readlen = PHYSFS_readBytes( f, (void*)raw_data, raw_data_length );

	if ( readlen != raw_data_length)
	{
		LOG_ERROR( "Error while reading worker file [%s], reason: %s", path,PHYSFS_getLastError() );
		PHYSFS_close(f);
		delete [] raw_data;
		raw_data = NULL;
		return;
	}
	PHYSFS_close(f);

	valid = true;

	// t = new thread( *(worker::_run), (void*)this );
	t = new thread( (worker::_run), (void*)this );
}

void worker::run_raw(const char* _raw)
{
	raw_data_length = strlen(_raw);
	raw_data = new char[raw_data_length+1]();
	strncpy( raw_data, _raw, raw_data_length );
	raw_data[raw_data_length] = '\0';
	path = "raw string"; // todo better path description in case of raw data
	valid = true;

	// jhonnyfix : t = new thread( *(worker::_run), (void*)this );
	t = new thread( (worker::_run), (void*)this );
}

void worker::run()
{

	if (!valid)
		return;

	// todo run lua worker!
	//printf("%s and im depth: %d\n", path, depth);
	//LOG_DEBUG( "Running worker: %s", path );

	if (init())
	{
		register_base();
	}	
	
	// load worker content in Lua state
	int load_res = luaL_loadbuffer( L, raw_data, raw_data_length, path );

	if (load_res == LUA_ERRSYNTAX)
	{
		LOG_ERROR( "Syntax error: %s", lua_tostring( L, -1 ) );

	} else if (load_res == LUA_ERRMEM) {
		LOG_ERROR( "Memory error: %s", lua_tostring( L, -1 ) );

	} else {

		// todo enter run-loop
		//printf("Good to go!\n");

		int s = lua_pcall(L, 0, LUA_MULTRET, 0);
		if (s == LUA_ERRRUN)
		{
			LOG_ERROR("Runtime Error: %s", lua_tostring(L, -1));
			return;
		} else if (s == LUA_ERRMEM)
		{
			LOG_ERROR("Memory Error: %s", lua_tostring(L, -1));
			return;
		} else if (s == LUA_ERRERR)
		{
			LOG_ERROR("Error running the error handler: %s", lua_tostring(L, -1));
			return;
		}

		while (running)
		{
			trigger_events();
		}

		LOG_DEBUG("Worker closing 0x%X", this);
		
	}

}


bool worker::init()
{
	// check if we already have a Lua state
	// todo

	L = luaL_newstate();
	if (L == NULL)
	{
		LOG_ERROR("Could not allocate memory while creating a new Lua worker.");
		return false;
	}

	return true;
}

void worker::register_base()
{

	// bind standard Lua libraries
	//luaL_openlibs( L );
	luaopen_physfs( L );

	// push internal pointers (used for key reg validation callback)
	lua_pushlightuserdata( L, this );
	lua_setfield( L, LUA_REGISTRYINDEX, LABRAT_APP_LUA_FIELD );
	//lua_setfield( L, LUA_GLOBALSINDEX, "_this" );

	// bind software key registration functions
	//lua_register( L, "labrat_keyreg_is_valid", labrat::_labrat_keyreg_is_valid );
	lua_register( L, "labrat_keyreg_validate", labrat::_labrat_keyreg_validate );

	// check previously stored key if valid
	if (_labrat_keyreg_is_valid())
	{
		this->register_extra();
	}

}

void worker::register_extra()
{

	// bind libraries after key validation
	// todo
	//printf("registering extra\n");

	static const luaL_Reg _core_lualibs[] = {
		{"", luaopen_base},
		{LUA_LOADLIBNAME, luaopen_package},
		{LUA_TABLIBNAME, luaopen_table},
		//{LUA_IOLIBNAME, luaopen_io},
		{LUA_OSLIBNAME, luaopen_os},
		{LUA_STRLIBNAME, luaopen_string},
		{LUA_MATHLIBNAME, luaopen_math},
		{LUA_DBLIBNAME, luaopen_debug},
		{NULL, NULL}
	};

	const luaL_Reg *lib = _core_lualibs;
	for (; lib->func; lib++) {
		lua_pushcfunction(L, lib->func);
		lua_pushstring(L, lib->name);
		lua_call(L, 1, 0);
	}

	luaopen_workers( L );
	luaopen_labrat_channels( L );

	// register external Lua modules via supplied callback
	if (register_external_cb)
		register_external_cb( L );
}

/*
void worker::send_message(const char* _channel_name, const char* _msg)
{
	std::tr1::unordered_map<std::string, channel*>::iterator it = channels.find(std::string(_channel_name));
	channel* _c;
	if (channels.end() == it )
	{
		// add channel to list
		channels[std::string(_channel_name)] = labrat::_labrat_get_channel(std::string(_channel_name));
	} else {
		_c = it->second;
	}
	
	_c->push(_msg);
}
*/

void worker::signal()
{
	lock_guard<mutex> guard(this->message_wait_mutex);

	// if ( this->messageCount == 0 )
	this->message_count++;
	this->message_cond.notify_one();
}

void worker::trigger_events()
{
	lock_guard<mutex> guard(this->message_wait_mutex);

	// if empty, stall until message
	while(this->message_count == 0)
		this->message_cond.wait(this->message_wait_mutex);

	std::vector<channel_connection*>::iterator it;
	
	for (it = connections.begin(); it != connections.end(); ++it)
	{
		(*it)->purge_messages();
	}

	this->message_count = 0;

	/*
	connection_mutex.lock();

	connection_mutex.unlock();
	*/

	// loop until empty message queue!
	/*worker_message* msg = message_queue.pop();
	while (msg != NULL)
	{

		// todo: LOG_INFO("Time to activate an event: %p %s", msg->sender, msg->data);
		if (event_refs.on_message != LUA_NOREF)
		{		
			lua_rawgeti( L, LUA_REGISTRYINDEX, event_refs.on_message );
			lua_pushstring( L, msg->data );
			int s = lua_pcall(L, 1, 0, 0);
			if (s)
			{
				LOG_ERROR("on_message error: %s", lua_tostring(L, -1));
				return;
			}
		}

		msg = message_queue.pop();
	}*/


}

void worker::add_channel_connection( channel_connection* _connection )
{

	connection_mutex.lock();
	connections.push_back( _connection );
	connection_mutex.unlock();

}

/*void worker::bind_event( int _ref )
{
	events_mutex.lock();
	event_refs.on_message = _ref;
	events_mutex.unlock();
}*/


//////////////////////////////////////////////////////////////////
// Lua bindings for worker class
//////////////////////////////////////////////////////////////////
int luaworkers_spawn(lua_State* L)
{
	// workers.spawn{ path = "test.lua" }

	// get calling worker pointer
	//lua_getfield( L, LUA_GLOBALSINDEX, "_this" );
	lua_getfield( L, LUA_REGISTRYINDEX, LABRAT_APP_LUA_FIELD );
	luaL_checktype(L, 2, LUA_TLIGHTUSERDATA );
	worker* _parent = (worker*)lua_touserdata(L, 2 );
	lua_pop( L, 1 );

	// create userdata object for new worker
	worker **_worker = (worker **)lua_newuserdata(L, sizeof(worker *));
	luaL_getmetatable(L, LUA_WORKERHANDLE);
	lua_setmetatable(L, -2);
	*_worker = new worker( _parent, NULL, _parent->register_external_cb );
	
	// get correct run-method and data
	luaL_checktype( L, 1, LUA_TTABLE );
	lua_getfield(L, 1, "path");

	if (!lua_isnil( L, 2 ))
	{
		LOG_DEBUG("Spawning worker from path: %s %p.", lua_tostring(L, -1), *_worker );
		(*_worker)->run_path( lua_tostring(L, -1) );
		lua_pop( L, 1 );

	} else {

		lua_pop( L, 1 );
		lua_getfield( L, 1, "data" );
		if (!lua_isnil( L, -1 ))
		{

			LOG_DEBUG("Spawning worker with raw data.");
			(*_worker)->run_raw( lua_tostring(L, -1) );
			lua_pop( L, 1 );

		} else {
			
			//luaL_error(L, "Invalid argument to workers.spawn(...).");
			LOG_ERROR("Invalid argument to workers.spawn(...).");
			delete (*_worker);
			return 0;
		}
		
	}

	wp->add(*_worker);

	return 1;
}

static int luaworkers_tostring (lua_State *L) {
	worker *_worker = *toworker(L);
	lua_pushfstring(L, "worker (%p)", _worker);
	return 1;
}

/*static int luaworkers_send (lua_State *L) {
	worker *_worker = *toworker(L);

	lua_getfield( L, LUA_GLOBALSINDEX, "_this" );
	luaL_checktype(L, -1, LUA_TLIGHTUSERDATA );
	worker* _parent = (worker*)lua_touserdata(L, -1 );
	lua_pop( L, 1 );

	_worker->send( new worker_message( _parent, luaL_checkstring(L, 2) ) );
	return 0;
}*/

/*static int luaworkers_onbind_self (lua_State *L) {

	lua_getfield( L, LUA_GLOBALSINDEX, "_this" );
	luaL_checktype(L, 2, LUA_TLIGHTUSERDATA );
	worker* _worker = (worker*)lua_touserdata(L, 2 );
	lua_pop( L, 1 );

	int r = luaL_ref(L, LUA_REGISTRYINDEX);
	if (r == LUA_REFNIL)
		LOG_ERROR("Reference to on_message was NIL!");
	_worker->bind_event( r );
	
	return 0;
}*/

static int luaworkers_close (lua_State *L) {

	lua_getfield( L, LUA_REGISTRYINDEX, LABRAT_APP_LUA_FIELD );
	worker* t_worker = (worker*)lua_touserdata(L, -1);
	lua_pop(L, 1);

	t_worker->stop();
	
	return 0;
}

int labrat::luaopen_workers(lua_State* L) {

	static const luaL_Reg workermeta[] = {
		//{"on", luaworkers_onbind},
		//{"send", luaworkers_send},
		//{"__gc", phys_gc}, // TODO add safe garbage collection
		{"__tostring", luaworkers_tostring},
		{NULL, NULL}
	};

	// create metatable for workers
	luaL_newmetatable(L, LUA_WORKERHANDLE);
	lua_pushvalue(L, -1);
	lua_setfield(L, -2, "__index");
	luaL_register(L, NULL, workermeta);  /* file methods */

	static const luaL_Reg _workerslib[] = {
	  {"spawn", luaworkers_spawn},
	  //{"on", luaworkers_onbind_self},
	  {"close", luaworkers_close},
	  {NULL, NULL}
	};

	luaL_register(L, "worker", _workerslib);

	return 0;
}


