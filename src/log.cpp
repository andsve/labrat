#include "log.h"
#include <cstdio>
#include <cstring>
#include <cstdarg>
#include "tinythread.h"

#ifdef _WIN32
	#include <windows.h>
#endif

using namespace labrat;
using namespace std;

static unsigned int _labrat_log_scopesize = 32;
static unsigned int _labrat_log_maxscopesize = 48;
static tthread::mutex _labrat_log_mutex;

void labrat::log(LogLevel _level, const char* _file, int _fileno, const char* _scope, const char* _message, ...)
{
	_labrat_log_mutex.lock();
	char buffer[4092];

	va_list va;
	va_start(va, _message);
	vsprintf(buffer, _message, va);
	va_end(va);
	
	if (strlen(_scope) > _labrat_log_scopesize)
		_labrat_log_scopesize = strlen(_scope);
	if (_labrat_log_scopesize > _labrat_log_maxscopesize)
		_labrat_log_scopesize = _labrat_log_maxscopesize;
		
#ifdef _WIN32

	// TODO Make LOG pretty print on Windows
	switch(_level)
	{
		case LOG_ERROR:
			SetConsoleTextAttribute(GetStdHandle(STD_ERROR_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_RED);
			fprintf(stderr, "E [%*s][ %s:%d - %s\n", _labrat_log_scopesize, _scope, _file, _fileno, buffer);
			break;
		case LOG_DEBUG:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN );
			printf("D [%*s][ %s:%d - %s\n", _labrat_log_scopesize, _scope, _file, _fileno, buffer);
			break;
		case LOG_WARNING:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE );
			printf("! [%*s][ %s:%d - %s\n", _labrat_log_scopesize, _scope, _file, _fileno, buffer);
			break;
		default:
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),FOREGROUND_INTENSITY | FOREGROUND_GREEN );
			printf("  [%*s][ %s:%d - %s\n", _labrat_log_scopesize, _scope, _file, _fileno, buffer);
			break;
	}

	// clear color
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), FOREGROUND_INTENSITY | FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE );

#else
	switch (_level)
	{
		case LOG_ERROR:
			fprintf(stderr, "E \033[1;31m[%*s]\033[0;31m %s:%d - %s\033[0m\n", _labrat_log_scopesize, _scope, _file, _fileno, buffer);
			break;
		case LOG_DEBUG:
			printf("D \033[1;33m[%*s]\033[0m %s:%d - %s\n", _labrat_log_scopesize, _scope, _file, _fileno, buffer);
			break;
		case LOG_WARNING:
			printf("! \033[1;29m[%*s]\033[0m %s\n", _labrat_log_scopesize, _scope, buffer);
			break;
		default:
			printf("  \033[1;32m[%*s]\033[0m %s\n", _labrat_log_scopesize, _scope, buffer);
			break;
	}
#endif

	_labrat_log_mutex.unlock();
	
}