#include <tinythread.h>
#include "keyreg.h"
#include "worker.h"

using namespace labrat;

const char* _labrat_keyreg_current_key = NULL;
tthread::mutex _labrat_keyreg_mutex;

bool labrat::_labrat_keyreg_is_valid()
{
	bool t_is_valid = false;
	_labrat_keyreg_mutex.lock();

	t_is_valid = true;

	// todo check key validity with _labrat_keyreg_current_key

	_labrat_keyreg_mutex.unlock();

	return t_is_valid;
}

int labrat::_labrat_keyreg_validate(lua_State* L)
{

	// get 'this' pointer
	lua_getfield( L, LUA_GLOBALSINDEX, "_this" );
	worker* _this = (worker*)lua_touserdata( L, -1 );
	lua_pop( L, 1 );

	// todo _labrat_keyreg_current_key 

	if ( _labrat_keyreg_is_valid() )
	{
		_this->register_extra();

		lua_pushboolean( L, true );

	} else {

		lua_pushboolean( L, false );

	}

	return 1;
}