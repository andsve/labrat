#include "channels.h"

static tthread::mutex channels_mutex;
static std::tr1::unordered_map<std::string, labrat::channel*> channels;

using namespace labrat;

/***
 * channel class methods
 */
bool labrat::channel::add(channel_connection* _con)
{
	//bool ret = true;
	m.lock();
	_con->c = this;
	connections.push_back( _con );
	m.unlock();
	return true;
}

void labrat::channel::push(const char* _msg, size_t _size)
{
	m.lock();

	channel_message* c_msg = new channel_message( _msg, _size );
	std::vector<channel_connection*>::iterator it = connections.begin();
	for (; it < connections.end(); ++it)
	{
		(*it)->add_message(c_msg);
	}

	m.unlock();

}

bool labrat::channel::unregister(worker* _worker)
{
	return true;
}

/***
 * aux channel functions
 */

labrat::channel* labrat::_labrat_get_channel(std::string _name)
{
	channel* t_c;
	channels_mutex.lock();
	t_c = channels[std::string(_name)];
	channels_mutex.unlock();
	return t_c;
}

/***
 * Lua bindings
 */

int labrat_channels_on(lua_State* L)
{
	// channels.on( channel_name, func )
	const char* t_name = luaL_checkstring( L, 1 );
	luaL_checktype( L, 2, LUA_TFUNCTION );
	int t_fun_id       = luaL_ref(L, LUA_REGISTRYINDEX);

	// get worker pointer
	lua_getfield( L, LUA_REGISTRYINDEX, LABRAT_APP_LUA_FIELD );
	worker* t_worker = (worker*)lua_touserdata(L, -1);
	lua_pop(L, 1);

	channels_mutex.lock();
	std::string _channel_name(t_name);
	std::tr1::unordered_map<std::string, channel*>::iterator it = channels.find(_channel_name);
	channel* _c;
	if (channels.end() != it )
	{
		LOG_DEBUG( "Subscribing to existing channel: %s (worker: 0x%X)", t_name, t_worker );
		_c = it->second;
	} else {
		LOG_DEBUG( "Creating new channel: %s (worker: 0x%X)", t_name, t_worker );
		_c = new channel( t_name );
		channels[_channel_name] = _c;
	}
	channels_mutex.unlock();

	_c->add( new channel_connection(t_worker, t_fun_id, _c ) );
	

	//channel* t_c = labrat::_labrat_get_channel(std::string(t_name));

	// TODO check if t_c is valid, otherwise create a new channel
	//t_c->add( new channel_connection(t_worker, t_fun_id, t_c ) );

	return 0;
}

int labrat_channels_send(lua_State* L)
{
	// channels.send( channel_name, message )
	const char* t_name = luaL_checkstring(L, 1);
	luaL_checktype( L, 2, LUA_TSTRING );

	size_t t_msg_len;
	const char* t_msg  = lua_tolstring( L, 2, &t_msg_len );

	//const char* t_msg  = luaL_checkstring(L, 2);
	//lua_getfield( L, LUA_REGISTRYINDEX, LABRAT_APP_LUA_FIELD );
	//worker* t_worker = (worker*)lua_touserdata(L, -1);
	//lua_pop(L, 1);

	channels_mutex.lock();
	std::string _channel_name(t_name);
	std::tr1::unordered_map<std::string, channel*>::iterator it = channels.find(_channel_name);
	channel* _c;
	if (channels.end() != it )
	{
		_c = it->second;
	} else {
		LOG_DEBUG( "Trying to push message on a channel with no listeners (channel: %s).", t_name );
		channels_mutex.unlock();
		return 0;
	}
	channels_mutex.unlock();

	_c->push(t_msg, t_msg_len);

	//t_worker->send_message(t_name, t_msg);

	/*channels_mutex.lock();
	channels[std::string(t_name)].register(t_name, channel_connection(t_worker, t_fun_id, NULL));
	channels_mutex.unlock();*/

	return 0;
}

int labrat::luaopen_labrat_channels(lua_State* L)
{

	static const luaL_Reg _workerslib[] = {
	  {"on", labrat_channels_on},
	  {"send", labrat_channels_send},
	  {NULL, NULL}
	};

	luaL_register(L, "channels", _workerslib);

	return 0;

}