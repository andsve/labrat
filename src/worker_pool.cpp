#include "labrat.h"
#include "log.h"
#include "worker_pool.h"

using namespace labrat;

worker_pool::worker_pool() : current_pool_index(0)
{

	if (wp != NULL)
		return;
	
	// set singleton
	wp = this;

	// setup worker list

}

worker_pool::~worker_pool()
{

}

void worker_pool::join_all()
{
	unsigned int current_join = 0;
	while (true)
	{
		pool_mutex.lock();
		if (current_pool_index <= current_join)
		{
			pool_mutex.unlock();
			return;
		}
		pool_mutex.unlock();

		pool[current_join]->join();
		pool_mutex.lock();
		current_join += 1;
		pool_mutex.unlock();
		
	}
}

void worker_pool::add(worker* _worker)
{
	pool_mutex.lock();
	pool[current_pool_index] = _worker;
	current_pool_index += 1;

	if (current_pool_index >= LABRAT_POOL_SIZE)
	{
		pool_mutex.unlock();
		printf("Ran out of space in pool, application failing.\n");
		exit(-1);
	}

	pool_mutex.unlock();

}


